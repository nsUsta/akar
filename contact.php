<?php
session_start();
// Verification si tentative d'envoi formulaire
$vqlid = true;
if (isset($_POST['submit'])) {


    //businessname

    //Nom
    if (!isset($_POST['name']) || trim($_POST['name']) == '') {
        $errors['name'] = 'Veuillez saisir un nom';
    }
    if (!isset($_POST['msg']) || trim($_POST['msg']) == '') {
        $errors['msg'] = 'Veuillez saisir un message';
    }

    if (!isset($_POST['subject']) || trim($_POST['subject']) == '') {
        $errors['subject'] = 'Veuillez saisir un sujet';
    }

    if (!(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))) {
        $errors['email'] = 'Veuillez saisir une adresse email valide';
    }
    if (empty($errors)) {
        $vqlid = true;

        $destinataire = 'contact@skysylver.net';

        $object = 'Ulker.fr' . $_POST['subject'];
        $headers = 'MIME-Version: 1.0'; // Version MIME
        $headers .= 'Content-type: text/html; charset=ISO-8859-1'; // l'en-tete Content-type pour le format HTML
        $headers .= 'De: <' . $_POST['email'] . '>'; // Expediteur
        $headers .= 'A: ' . $destinataire . '\n'; // Destinataire

        $message = 'De : <' . $_POST['email'] . '> \nMessage : ' . $_POST['msg'];

        //$message = '<div style="width: 100%; text-align: center; font-weight: bold"> Bonjour' .$_POST['name'] .'! <br>' .'</div>';

        // Envoi du message
        if (mail($destinataire, utf8_encode($object), utf8_encode($message), $headers)) {
            $res = '<p class="bg-success text-center rounded text-white">Votre message a bien été envoyé</p></section>';

        } else // Non envoyé
        {
            $res = '<p class="bg-danger text-center rounded text-white">Erreur lors de l\'envoi de votre message<br> Veuillez reessayer 
plus tard</p>';
        }
    } else $vqlid = false;

}
?>


<!DOCTYPE html>
<html>
<head>
    <?php include 'inc/header.php'; ?>
</head>

<body>
<?php require './inc/menu.php'; ?>

<div id="banner2" class="banner"></div>
<section class="mt-2">

    <h1>Contactez-nous</h1>
    <p>Akar Nord France - Biscuit turc halal<br></p>

    <p class="location">
        Distributeur ulker france<br>
        51 rue du commandant Rolland<br>
        93350 Le Bourget<br>
    </p>

    <p class="mail"><a class="" href="mailto:contact@ulker.fr">contact@ulker.fr</a></p><br><br>

</section>
<h5>Ou contactez-nous via formulaire :</h5>


<!-- Affichage du formulaire
     Champs en rouge
     Textes sous le formulaire -->

<?php if (isset($res)) echo $res; ?>
<form method="post" action="#">
    <div class="form-group">
        <label for="name">Nom :</label>
        <input type="text" required="required" name="name" class="form-control <?php
        if (isset($errors['name'])) {
            echo ' is-invalid"><div class=invalid-feedback >' . $errors['name'] . "</div>";
        } elseif (!$vqlid) {
            echo ' is-valid" value="' . $_POST['name'] . '"><div class=valid-feedback >Nom valide</div>';
        } else echo '">';
        ?>
    </div>

    <div class="form-group">
        <label for="businessName">Entreprise :</label>
        <input type="text" required="required" name="businessName" class="form-control <?php
        if (isset($errors['businessName'])) {
            echo ' is-invalid"><div class=invalid-feedback >' . $errors['businessName'] . "</div>";
        } elseif (!$vqlid) {
            echo ' is-valid" value="' . $_POST['businessName'] . '"><div class=valid-feedback >Entreprise valide</div>';
        } else echo '">';
        ?>

    </div>

    <div class="form-group">
        <label for="email">Email :</label>
        <input type="email" required="required" name="email" class="form-control <?php
        if (isset($errors['email'])) {
            echo ' is-invalid"><div class=invalid-feedback >' . $errors['email'] . "</div>";
        } elseif (!$vqlid) {
            echo ' is-valid" value="' . $_POST['email'] . '"><div class=valid-feedback >Email valide</div>';
        } else echo '">';
        ?>
    </div>
    <div class="form-group">
        <label for="subject">Sujet :</label>
        <input type="text" required="required" name="subject" class="form-control <?php
        if (isset($errors['subject'])) {
            echo ' is-invalid"><div class=invalid-feedback >' . $errors['subject'] . "</div>";
        } elseif (!$vqlid) {
            echo ' is-valid" value="' . $_POST['subject'] . '"><div class=valid-feedback >Nom valide</div>';
        } else echo '">';
        ?>
    </div>

    <div class="form-group">
        <label for="msg">Message :</label>
        <textarea name="msg" required="required" class="form-control <?php
        if (isset($errors['subject'])) {
            echo ' is-invalid"></textarea><div class=invalid-feedback >' . $errors['msg'] . "</div>";
        } elseif (!$vqlid) {
            echo ' is-valid">' . $_POST['msg'] . '</textarea><div class=valid-feedback >Nom valide</div>';
        } else echo '"></textarea>'
        ?>
    </div>
    <input type="submit" name="submit" class="form-control mb-3" value="Envoyer"/>
</form>
</body>
</html>