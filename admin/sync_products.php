<?php
/**
 * Created by PhpStorm.
 * User: enesa
 * Date: 14/08/2019
 * Time: 01:01
 */
//echo $_SERVER['DOCUMENT_ROOT'];

require '../inc/db.php';

$d = $local->prepare("SELECT id, last_sync as ls FROM last_syncs WHERE name = 'products'");
$d->execute();
$temp1 = $d->fetch();



// Ajout / Maj des produits
$temp = $akar->prepare("SELECT id, ref_prd, lib_prd, cod_fam_prd_path, unite, packet_unit, packet_qty, contenu, unite_contenu, id_taxclass, b_actif FROM produits WHERE last_synch_time_pos >= ?");
$temp->execute([$temp1->ls]);
?>
<table>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
<?php
while ($res = $temp->fetch()) {
    $d = $local->prepare("INSERT INTO `products`(`id`, `ref_prd`, `lib_prd`, `cod_fam_prd_path`, `unite`, `packet_unit`, `packet_qty`, `contenu`, `unite_contenu`, `id_taxclass`, `b_actif`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE ref_prd = VALUES(ref_prd), lib_prd = VALUES(lib_prd) , cod_fam_prd_path = VALUES(cod_fam_prd_path), unite = VALUES(unite), packet_unit = VALUES(packet_unit), packet_qty = VALUES(packet_qty), contenu = VALUES(contenu), unite_contenu = VALUES(unite_contenu), id_taxclass = VALUES(id_taxclass), b_actif = VALUES(b_actif)");
    $d->execute([$res->id, $res->ref_prd, $res->lib_prd, $res->cod_fam_prd_path, $res->unite, $res->packet_unit, $res->packet_qty, $res->contenu, $res->unite_contenu, $res->id_taxclass, $res->b_actif]);
    echo 'Id : ' .$res->id .' - REf : ' .$res->ref_prd .' - Libelle produit: ' .$res->lib_prd .' - Code famille produit : ' .$res->cod_fam_prd_path .' - Unite : ' .$res->unite .' - Packet Unite : ' .$res->packet_unit .' - Paquet qty : ' .$res->packet_qty .' - Contenu : ' .$res->contenu .' - Unite Contenu : ' .$res->unite_contenu .' - Classe Taxe : ' .$res->id_taxclass .'<br>';

}


//Ajout / maj des prix
$temp = $akar->prepare("SELECT id_prd, max(prix_u_ht) as p FROM tarifs_produits WHERE dat_upd >= ? GROUP BY id_prd");
$temp->execute([$temp1->ls]);
while($res = $temp->fetch()){
    $d = $local->prepare("UPDATE products SET prix_u_ht = ? WHERE id = ?");
    $d->execute([$res->p, $res->id_prd]);
}


$temp = $akar->prepare("SELECT id_prd, qte_stck, max(dat_upd) FROM stk_inventory_details WHERE dat_upd >= ? GROUP BY id_prd");
$temp->execute([$temp1->ls]);
while($res = $temp->fetch()){
    $d = $local->prepare("UPDATE products SET qte_stck = ? WHERE id = ?");
    $d->execute([$res->qte_stck, $res->id_prd]);
    echo 'Id prd :' .$res->id_prd .' - Quantite stock : ' .$res->qte_stck;
}






$temp = $local->prepare("UPDATE `last_syncs` SET `last_sync`= CURRENT_TIMESTAMP WHERE id = ?");
$temp->execute([$temp1->id]); ?>

</table>
<a href="admin.php">Retourner au panel</a>
