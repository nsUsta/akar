<!DOCTYPE html>
<html>

<head>
    <!--
    <title>Ülker - Espace Administrateur</title>
    <link rel="stylesheet" type="text/css" href="style/style.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="style/<?php echo basename($_SERVER['PHP_SELF'], ".php");; ?>.css" media="all"/>
    <link rel="icon" type="image/x-icon" href="../style/images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
-->
    <?php
    require '../inc/header.php';
    require_once '../inc/db.php';
    ?>

</head>
<body>

<div class="text-body">
    <h3>Mises a jours :</h3>

    <?php
        $tmp = $local->prepare("SELECT name, last_sync FROM last_syncs");
        $tmp->execute([]);
    ?>
    <table>
        <tr>
            <th>Table</th>
            <th>Derniere synchronisation</th>
            <th></th>
        </tr>

    <?php
        while($res = $tmp->fetch()){
            echo '<tr><th>' .$res->name .'</th><th>' .$res->last_sync .'</th><th><a href=sync_' .$res->name .'.php><button>Mettre a jour</button></a> </th></tr>';
        }

    ?>
    </table>

</div>


</body>
</html>