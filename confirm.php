<?php
if (isset($_GET['id']) && isset($_GET['token'])) {

    require './inc/db.php';
    $temp = $local->prepare("SELECT validation_token FROM customers WHERE id = ? AND validation_token != NULL");
    $temp->execute([$_GET['id']]);

    if ($res = $temp->fetch()) {
        if ($res->validation_token == $_GET['token']) {
            // Valide
            $temp2 = $local->prepare("UPDATE `customers` SET `validation_token`= NULL WHERE id = ?");
            $temp2->execute([$_GET['id']]);

            $_SESSION['flash']['valid'] = "Votre adresse Email a bien été validée. La validation de votre compte par nos services sera effectuée sous 48 heures.";
            Header('Location: login.php');
            // Toek Invalide
        }
        // Id invalide
    }
}
$_SESSION['flash']['invalid'] = "Ce lien d'activation est invalide !";
Header('Location: login.php');
?>
2