<?php
session_start();
if (isset($_SESSION['connected']) && $_SESSION['connected'] === true) {
    Header('Location: account.php');
}


if (!empty($_POST)) {
    // Vide
    if (empty($_POST['mail'])) {
        $errors['mail'] = "Vous n'avez pas saisi d'adresse mail";
    }
    if (empty($_POST['password'])) {
        $errors['password'] = "Vous n'avez pas saisi de mot de passe";
    }


    if (!isset($errors)) {
        // Chargement bdd
        require './inc/db.php';
        $test = $local->prepare("SELECT * FROM users WHERE mail = ?");
        $test->execute([$_POST['mail']]);

        if ($res = $test->fetch()) {

            if (password_verify($_POST['password'], $res->password)) {

                // SESSION LOGIN
                $_SESSION['connected'] = true;
                $_SESSION['name'] = $res->name;
                $_SESSION['surname'] = $res->surname;
                $_SESSION['society'] = $res->society;
                $_SESSION['sir'] = $res->sir;
                $_SESSION['mail'] = $res->mail;


                Header('Location : account.php');

            } else {
                $errors['incorrect'] = "L'identifiant ou le mot de passe est incorrect";
            }
        } else {
            $errors['incorrect'] = "L'identifiant ou le mot de passe est incorrect";
        }
    }
}

?>


<html>
<head>
    <title>Ülker - Connexion</title>
    <link rel="stylesheet" type="text/css" href="style/<?php echo basename($_SERVER['PHP_SELF'], ".php"); ?>.css"
          media="all"/>
    <link rel="icon" type="image/x-icon" href="style/images/favicon.png"/>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
</head>

<body>
<div class="center">
    <a href="home.php"><img src="./style/images/logo.png" alt="Logo Ülker" id="Logo"/></a>
</div>

<span class="center">
        <h2>Identifiez-vous</h2>
    </span>
<div id="flex">
    <div class="cadre">
        <div class="titre-cadre">
            <legend>Je suis déjà client Ülker</legend>
        </div>
        <div class="contenu-cadre">
            <form action="#" method="POST">
                <input type="email" name="mail" placeholder="Adresse mail" required><br>
                <input type="password" name="password" placeholder="Mot de passe" required><br>
                <input type="submit" class="button" value="Connexion">
            </form>
        </div>
    </div>


    <div class="cadre">
        <div class="titre-cadre">
            <legend> Je n'ai pas encore de compte Ülker</legend>
        </div>
        <div class="contenu-cadre">
            <a href="register.php"><input type="button" class="button" value="S'inscrire"/></a>
        </div>
    </div>
</div>
<?php
if (!empty($_POST)) {
    if (!empty($errors)) {
        echo '<br><br><div class="bg-error">';
        foreach ($errors as $error):
            echo $error . '<br>';
        endforeach;
        echo '</div>';
    }
}
?>
</body>
</html>
