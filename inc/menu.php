<header>
    <!-- Logo -->
    <img src="./style/images/logo.png" alt="Logo Ülker" id="Logo"/>
    <!-- Menu -->
    <div class="topnav" id="myTopnav">
        <a href="./home.php">Accueil</a>
        <a href="./shop.php">Boutique</a>
        <a href="./contact.php">Contact</a>
        <?php
        if (isset($_SESSION['connected']) && $_SESSION['connected'] === true)
            echo '<a href="account.php" class="mr-0">Espace Client</a>';
        else {
            echo '<a href="login.php" class="mr-0">Connexion</a>
                 <a href="register.php" class="mr-0">Inscription</a>';
        }
        ?>
        <a href="javascript:void(0);" class="icon" onclick="menu()">
            <i class="fa fa-bars"></i></a>
    </div>


    <script>
        let sPage = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);
        document.querySelector("a[href='./" + sPage + "']").className += " active";

        function menu() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
</header>