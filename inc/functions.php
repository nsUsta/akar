<?php
    function debug($variable){
        
        echo '<pre>' . print_r($variable, true) . '</pre>';
    }

    function gen_token($length){
        $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        return substr(str_shuffle(str_repeat($chars, $length)), 0, $length);
    }

    function getPageName($pagename){
    switch ($pagename){
        case "shop":
            return " - Boutique";
        case "register":
            " - Inscription";
        case "home":
            return " - Accueil";
        case "contact":
            return " - Contact";
        case "account":
            return " - Mon compte";

    }

    }
?>