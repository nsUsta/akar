<title>Ülker<?php if(isset($title)) echo ' - ' .$title; ?></title>

<link rel="stylesheet" type="text/css" href="./style/bootstrap/bootstrap.min.css" crossorigin="anonymous"/>
<link rel="stylesheet" type="text/css" href="./style/style.css" media="all"/>
<link rel="stylesheet" type="text/css" href="./style/<?= basename($_SERVER['PHP_SELF'], ".php"); ?>.css" media="all"/>
<link rel="icon" type="image/x-icon" href="style/images/favicon.png"/>
<link rel="stylesheet" type="text/css" href="./style/font-awesome.min.css"/>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" context="text/html; charset=utf-8">

<?php mb_internal_encoding("UTF-8"); ?>
