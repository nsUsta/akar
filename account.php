<?php
session_start();
if (isset($_GET['tab']) && $_GET['tab'] == 'disconnect') session_destroy();
include 'inc/header.php';
$title = 'Votre compte'; ?>
<div class="sidemenu">
    <ul id="list">
        <a class="active" href="#tab=infos" id="infos" onclick="set_active('infos')">
            <li>Mes informations</li>
        </a>
        <a id="commands" href="#tab=commands" onclick="set_active('commands')">
            <li>Mes commandes</li>
        </a>
        <a id="factures" href="#tab=factures" onclick="set_active('factures')">
            <li>Mes factures</li>
        </a>
        <a>
            <li>Moyens de paiement</li>
        </a>
        <a id="disconnect" href="?tab=disconnect" onclick="set_active('disconnect')">
            <li>Déconnexion</li>
        </a>
    </ul>
</div>

<script type="text/javascript">
    function set_active(str) {

        document.getElementsByClassName("active").item(1).className = "";
        document.getElementById(str).className = "active";
    }
</script>


<?php include 'inc/footer.php'; ?>
