<?php
session_start();
if (isset($_SESSION['connected']) && $_SESSION['connected'] === true) {
    Header('Location: account.php');
}
require_once 'inc/db.php';

if (!empty($_POST)) {
    require_once 'inc/functions.php';

// Nom
    if (empty($_POST['name'])) $errors['name'] = "Votre nom n'a pas été saisi !";
    else if (!(preg_match('/^[a-zA-ZéèëôöêùÜÖà]{3,32}$/', $_POST['name']))) $errors['name'] = "Votre nom ne peut contenir que des lettres et doit en comporter entre 3 et 32.";

// Prénom
    if (empty($_POST['surname'])) $errors['surname'] = "Veuillez saisir un prénom";
    else if (!(preg_match('/^[a-zA-ZéèëôöêùÜÖà]{3,32}$/', $_POST['surname']))) $errors['surname'] = "Votre nom ne peut contenir que des lettres et doit en comporter entre 3 et 32.";

// Société
    if (empty($_POST['society'])) $errors['society'] = "Le nom de votre societe n'a pas ete saisi";
    else if (!(preg_match('/^[a-zA-Z0-9]*$/', $_POST['society']))) $errors['society'] = "Le nom de la societe ne peut contenir que des lettres et des chiffres";

// email
    if (!(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))) $errors['email'] = "Veuillez saisir une adresse mail valide";
    else {
        $test = $local->prepare("SELECT `mail` FROM customers WHERE mail = ?");
        $test->execute([$_POST['email']]);
        if ($res = $test->fetch()) $errors['email'] = "Cette adresse mail est déjà utilisée";
    }

// Mot de passe
    if (empty($_POST['password'])) $errors['password'] = "Veuillez saisir un mot de passe";
    else if ($_POST['password'] != $_POST['password_confirm']) $errors['password'] = "Vos mots de passe de correspondent pas";
    else if (!preg_match('/^.{8,32}$/', $_POST['password'])) $errors['password'] = "Votre mot de passe doit contenir entre 8 et 32 caractères";
    else if (!preg_match('/[A-Z]+/', $_POST['password']) || !preg_match('/[a-z]+/', $_POST['password']) || !preg_match('/[0-9]+/', $_POST['password'])) $errors['password'] = "Votre mot de passe doit contenir au moins une majuscule et un chiffre";


// Fichiers
    if (!empty($_FILES['KBIS']['tmp_name'])) {
        if (is_uploaded_file($_FILES['KBIS']['tmp_name'])) {
            if (!@getimagesize($_FILES['KBIS']['tmp_name'])) $errors['KBIS'] = 'Le fichier n\'est pas une image';
            else if ($_FILES['KBIS']['size'] > 500000) $errors['KBIS'] = 'La taille du fichier ne doit pas exceder 5 Mo';
        } else $errors['KBIS'] = 'Le fichier n\'est pas une image';
    } else $errors['KBIS'] = 'Veuillez joindre un scan du KBIS de votre societe';

    if (!empty($_FILES['PI']['tmp_name'])) {
        if (is_uploaded_file($_FILES['PI']['tmp_name'])) {
            if (!@getimagesize($_FILES['PI']['tmp_name'])) $errors['PI'] = 'Le fichier n\'est pas une image';
            else if ($_FILES['PI']['size'] > 500000) $errors['PI'] = 'La taille du fichier ne doit pas exceder 5 Mo';
        } else $errors['PI'] = 'Le fichier n\'est pas une image';
    } else $errors['PI'] = 'Veuillez joindre un scan de votre piece d\'identite';



    //*******************************************************//

    // FORMULAIRE VALIDE
    if (!isset($errors)) {
        //insertion customers
        $token = '1';
        $req = $local->prepare("INSERT INTO `customers`(`mail`, `password`, `name`, `surname`, `society`, `validation_token`) VALUES (?,?,?,?,?,?)");
        $req->execute([$_POST['email'], password_hash($_POST['password'], PASSWORD_DEFAULT), $_POST['name'], $_POST['surname'], $_POST['society'], $token]);

    // Fichier KBIS
        $temp = explode(".", $_FILES["KBIS"]["name"]);
        $name = 'KBIS.' . $_POST['email'] . '.' . end($temp);
        if (!move_uploaded_file('./uploads/' .$_FILES["KBIS"]["tmp_name"], $name)) {
            echo 'Erreur lors de l\'envoi du KBIS';
        }
    // Fichier Piece d'identité
        $temp = explode(".", $_FILES["PI"]["name"]);
        $name = 'PI.' . $_POST['email'] . '.' . end($temp);
        if (!move_uploaded_file('./uploads/' .$_FILES["PI"]["tmp_name"], $name)) {
            echo 'Erreur lors de l\'envoi du KBIS';
        }





        //Infos pour envoi mail
        $id = $local->lastInsertId();
//        $temp = $local->prepare("SELECT * FROM customers WHERE id = ?");
//        $temp->execute($id);

        // Préparation mail
        //$to = 'contact@skysylver.net';
        $to = $_POST['email'];
        $subject = "Akar - Confirmation de votre compte";
        $htmlContent = file_get_contents("email_template.html");

        $htmlContent = preg_replace("#%id#", $id, $htmlContent);
        $htmlContent = preg_replace("/%token/", $token, $htmlContent);
        $htmlContent = preg_replace("/%name/", $_POST['name'], $htmlContent);
        $htmlContent = preg_replace("/%surname/", $_POST['surname'], $htmlContent);

// Set content-type header for sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Akar<validation@skysylver.net>' . "\r\n";

// Send email
        //mail($_POST['email'], "Ulker - Confirmer votre compte", $message_txt, $header);
        if(mail($to, $subject, $htmlContent, $headers)){
            $_SESSION['flash']['valid'] = "Votre inscription à été prise en compte";
        }else{
            echo 'Email sending failed.';
        }
        Header('Location: home.php');
        exit();
    }
}
?>
<html>
<head>
    <title>Ülker - Inscription</title>
    <link rel="stylesheet" type="text/css" href="./style/bootstrap/bootstrap.min.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="./style/style.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="style/<?php echo basename($_SERVER['PHP_SELF'], ".php"); ?>.css"
          media="all"/>
    <link rel="icon" type="image/x-icon" href="style/images/favicon.png"/>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
</head>

<body>
<div class="center">
    <a href="home.php"><img src="./style/images/logo.png" alt="Logo Ülker" id="Logo"/></a>
</div>
<span class="center"><h2>Créer un compte  </h2></span>


<form action="#" method="POST" enctype="multipart/form-data">
    <div id="form">
        <div class="form-group">
            <label for="name">Nom</label>
            <input type="text" name="name" class="form-control <?php
            if (isset($errors['name'])) echo 'is-invalid" required><div class="invalid-feedback">' . $errors["name"] . '</div>';
            else if (isset($_POST['name'])) echo 'is-valid" value="' . $_POST["name"] . '" required>';
            else echo '" required>';
            ?>
        <div class=" valid-feedback">Nom valide</div>
    </div>

    <div class="form-group">
        <label for="surname">Prénom</label>
        <input type="text" name="surname" class="form-control <?php
        if (isset($errors['surname'])) echo 'is-invalid" required><div class="invalid-feedback">' . $errors["surname"] . '</div>';
        else if (isset($_POST['surname'])) echo 'is-valid" value="' . $_POST["surname"] . '" required>';
        else echo '" required>';
        ?>
            <div class=" valid-feedback">Prénom valide</div>
    </div>

    <div class="form-group">
        <label for="society">Société</label>
        <input type="text" name="society" class="form-control <?php
        if (isset($errors['society'])) echo 'is-invalid" required><div class="invalid-feedback">' . $errors["society"] . '</div>';
        else if (isset($_POST['society'])) echo 'is-valid" value="' . $_POST["society"] . '" required>';
        else echo '" required>';
        ?>
            <div class=" valid-feedback">Nom de société valide</div>
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control <?php
        if (isset($errors['email'])) echo 'is-invalid" required><div class="invalid-feedback">' . $errors["email"] . '</div>';
        else if (isset($_POST['email'])) echo 'is-valid" value="' . $_POST["email"] . '" required>';
        else echo '" required>';
        ?>
            <div class=" valid-feedback">Email valide</div>
    </div>

    <div class="form-group">
        <label for="password">Mot de passe</label>
        <input type="password" name="password" class="form-control <?php
        if (isset($errors['password'])) echo 'is-invalid" required><div class="invalid-feedback">' . $errors["password"] . '</div>';
        else if (isset($_POST['password'])) echo 'is-valid" value="' . $_POST["password"] . '" required>';
        else echo '" required>';
        ?>
        <div class="valid-feedback">Mot de passe valide</div>
    </div>


    <div class="form-group">
        <label for="password_confirm">Confirmez votre mot de passe</label>
        <input type="password" name="password_confirm" class="form-control <?php
        if (isset($errors['password'])) echo 'is-invalid" required><div class="invalid-feedback">' . $errors["password"] . '</div>';
        else if (isset($_POST['password_confirm']
        )) echo 'is-valid" value="' . $_POST["password_confirm"] . '" required>';
        else echo '" required>';
        ?>
        <div class="valid-feedback">Mot de passe valide</div>
    </div>


    <div class="file-akar">
        <label class="custom-file form-group">
            <input type="file" name="KBIS" id="KBIS" class="custom-file-input <?php if (isset($errors['KBIS'])) echo 'is-invalid';?>" required>
            <label for="KBIS" class="custom-file-label">KBIS de la societe</label>
            <div class="invalid-feedback"><?php if (isset($errors['KBIS'])) echo $errors['KBIS'];?></div>
        </label>
    </div>

    <div class="file-akar">
        <label class="custom-file form-group">
            <input type="file" name="PI" id="PI" class="custom-file-input<?php if (isset($errors['PI'])) echo ' is-invalid';?>" required>
            <label for="PI" class="custom-file-label">Pièce d'identité du gérant</label>
            <div class="invalid-feedback"><?php if (isset($errors['PI'])) echo $errors['PI']; ?></div>
        </label>
    </div>


    <br>
    <button type="submit" name="submit" class="submit">Envoyer</button>
</form>
<?php
if (!empty($_POST)) {
    if (!empty($errors)) {
        echo '<div class = "error"><br><br>';
        foreach ($errors as $error):
            echo $error . '<br>';
        endforeach;
        echo '</div>';
    }
}
?>

<script src="./js/jquery-1.12.4.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('input').change(function (e) {
                e.target.classList.remove('is-invalid');
            })
            $('.custom-file-input').change(function(e){
                e.target.nextElementSibling.textContent = e.target.files[0].name;
            });
        });
</script>
</body>
</html>
